# Pytorch packages
import torch
from torchvision import transforms

# Numpy for intermediate
import numpy as np

# To open digital pathology images
import openslide
import tifffile

# Standard imports
import sys
import glob
import cv2
import os
import random
import time

# PIL
from PIL import Image
from PIL import ImageStat

# For status bar (might remove)
from tqdm import tqdm

class RoiBuilder():
    '''
     Class object that handles the organization, labeling, tile generation and cacheing of whole slide images.
     Usage:
        init(): Initialize the RoiBuilder.  Checks for cache files based on image name (must be unique). Some metadata is filled automatically.
                status will either be VALID or CACHE MISSING
        build(): Will trigger raster scan into tiles and L0 RoI identification based on pixel counts and statistics.
                MUST be called if status is CACHE MISSING before calling .generate() or .get() methods
                If successful status will be VALID
        update_resolution_and_buffer(): Define the image transforms, with output INPUT_DIMENSIONS
                If successful, status will be set to VALID-READY
        get_train_data(): Generate a 4D tensor of tiled images from the input WSI cache
                raises: RuntimeError if a cache or transform is missing

     Arguements:
        PATH_IMG: Full WSI path
        params: Dictonary of custom, user parameters (e.g. for caMIC), and outcome data (e.g. a outcome tensor, or list of outcomes per tile)
    '''
    # =============================================================================
    # Generic access functions
    def __init__(self, PATH_IMG, params):

        # Default values
        self.params = {}
        self.params['roi_size'] = 1200
        self.params['padding'] = 0
        self.params['root_cache_dir'] = '.' 
        self.params['dropout'] = 0.9 # Defuct
        self.params['dtype'] = 'float32' # Defuct

        # Fill/overwrite defaults
        for key in params: self.params[key] = params[key] 

        # Base name should always be unique, comes from file name
        self.params['fullpath'] = PATH_IMG
        self.params['basename'] = os.path.split(PATH_IMG)[1].split('.')[0]
        self.params['coor_cache'] = '{0}/coor_{1}_rois_size{2}_hsvcut_v3.npy'.format(self.params['root_cache_dir'] , self.params['basename'], self.params['roi_size'])
        self.params['data_cache'] = '{0}/data_{1}_rois_size{2}_hsvcut_v3.npy'.format(self.params['root_cache_dir'] , self.params['basename'], self.params['roi_size'])
        self.params['ntiles'] = -1
        
        # Keep track of internal status
        self.status = {'INIT':0, 'CACHE':0, 'READY':0}

        print ('-+-----------------------------------------------------------------------------------------------------------')
        print ("ROI builder for file at " + self.params['fullpath'])
        print (" | Checking....")

        self.loud = False

        if  (os.path.isfile(self.params['data_cache'])):
            raster = np.load(self.params['coor_cache'])
            print (" | ROI Builder is VALID with {0} tiles".format(len(raster)))
            self.params['ntiles'] = len(raster)
            self.status['CACHE'] = 1
        else:
            print ("This will require building of ROIs.")
            self.status['CACHE'] = 0

        self.caMIC_eligable = False
        print ("Can this be opened in openside?")
        print (" | Checking...")
        try:
            openslide.OpenSlide(self.params['fullpath'])
            print (" | Yes!!")
            self.params['caMIC_eligable'] = True
        except:
            print (" | Nope!!")
            self.params['caMIC_eligable'] = False
        self.print()
        print ('-+-----------------------------------------------------------------------------------------------------------')

    def print(self):
        print ("RoiBuilder parameter summary:")
        for key in self.params: print (" | {0:16s} = {1}".format(key, self.params[key]))
        self.status['INIT'] = 1
        print ("RoiBuilder status summary:")
        for key in self.status: print (" | {0:16s} = {1}".format(key, self.status[key]))


    def update_cache_dir(self, new_cache_dir):
        self.params['root_cache_dir'] = new_cache_dir
        self.params['coor_cache'] = '{0}/coor_{1}_rois_size{2}_hsvcut_v3.npy'.format(self.params['root_cache_dir'] , self.params['basename'], self.params['roi_size'])
        self.params['data_cache'] = '{0}/data_{1}_rois_size{2}_hsvcut_v3.npy'.format(self.params['root_cache_dir'] , self.params['basename'], self.params['roi_size'])


    def getsize(self):
        '''
        Get the number of slide tiles
        '''
        return self.params['ntiles']

    def getname(self):
        '''
        The basename
        '''
        return self.params['basename']

    def getmeta(self):
        return self.params

    @staticmethod
    def get_maxsized_series(tfile):
        '''
        Get the largest tiff file page

        Returns: Series index with the largest cooresponding ndarray 
        Parameters:
            tfile: Tifffile tiffile
        '''
        biggest_size = 0
        for i,series in enumerate(tfile.series):
            print ("Testing series at [{0}]".format(i))
            img = tfile.asarray(series=series)
            if np.prod(img.shape) > biggest_size:
                biggest_size = np.prod(img.shape)
                target_series = i
            del img
        return target_series

    @staticmethod
    def array_read_region(arr, coord, size):
        '''
        Make a numpy array act like an openslide image, pull a tile from the whole array

        Returns: raw_slice which is a uint8 numpy array and roi_slice which is a PIL image
        Parameters:
            arr: Array to write to
            coord: Convention corner coordinate of tile
            size: Size of tile
        '''
        raw_slice = arr[coord[0] : coord[0] + size[0], coord[1] : coord[1] + size[1], :]
        roi_slice = Image.fromarray(raw_slice)
        return roi_slice, raw_slice

    @staticmethod
    def array_write_region_2d(arr, coord, size, value):
        '''
        Write to region

        Parameters:
            arr: Array to write to
            coord: Convention corner coordinate of tile
            size: Size of tile
            value: Broadcastable array
        '''
        arr[coord[0] : coord[0] + size[0], coord[1] : coord[1] + size[1]] = value

    # =============================================================================
    def set_model_transform(self, transform, model, post, downsample):
        self.status['TRANSFORM'] = 1
        self.pre       = transform 
        self.model     = model 
        self.post      = post 
        self.downsample = downsample 

    def slide_to_tiff(self, channels):
        if not self.status['TRANSFORM']: raise RuntimeError("You called slide_to_tiff without setting your pre, model, and post transformations")
        channels = range(channels)
        self.params['roi_size'] = 256 
        self.params['padding']  = 20000
        INPUT_STEPSIZE =  int ( self.params['roi_size'] )
        padding         = int ( self.params['padding']  )
        OUTPUT_STEPSIZE = int ( INPUT_STEPSIZE / self.downsample )

        # Origional image
        tfile = tifffile.TiffFile(self.params['fullpath'])
        target_series = self.get_maxsized_series(tfile)
        img = tfile.asarray(series=target_series)

        print ("Selected series index (guessing to be 40x): ", target_series)
        print ("Selected series size  (guessing to be 40x): ", img.shape)

        INPUT_DIMENSIONS = img.shape
        xsteps = range(0 + padding, INPUT_DIMENSIONS[0] - INPUT_STEPSIZE - padding - 1, INPUT_STEPSIZE) 
        ysteps = range(0 + padding, INPUT_DIMENSIONS[1] - INPUT_STEPSIZE - padding - 1, INPUT_STEPSIZE)
        raster = [ (x, y) for y in ysteps for x in xsteps]

        OUTPUT_DIMENSIONS = (OUTPUT_STEPSIZE*len(xsteps), OUTPUT_STEPSIZE*len(ysteps)) 
        output_xsteps = range(0, OUTPUT_DIMENSIONS[0] , OUTPUT_STEPSIZE) 
        output_ysteps = range(0, OUTPUT_DIMENSIONS[1] , OUTPUT_STEPSIZE)
        output_raster = [ (x, y) for y in output_ysteps for x in output_xsteps]

        dla   = []
        red   = np.empty( OUTPUT_DIMENSIONS, dtype='uint8')
        green = np.empty( OUTPUT_DIMENSIONS, dtype='uint8')
        blue  = np.empty( OUTPUT_DIMENSIONS, dtype='uint8')
        for chan in channels: dla.append ( np.empty( OUTPUT_DIMENSIONS, dtype='uint8') )
        hard_sums = torch.zeros(len(channels)).cuda()

        print ("X and Y Steps: ", len(output_xsteps), len(output_ysteps))
        print ("Input size: ",  INPUT_DIMENSIONS)
        print ("Output size: ", OUTPUT_DIMENSIONS)
        print ("Raster Length: ", len(raster), len(output_raster))

        pbar = tqdm(range(len(raster)))
        with torch.no_grad():
            for roi_coord, target_coord in zip(raster, output_raster):
                # Read tile
                roi, data = self.array_read_region(img, roi_coord,    (INPUT_STEPSIZE,INPUT_STEPSIZE))

                # Apply transformations
                dla_vals =  self.post (self.model ( self.pre(roi).unsqueeze(0))).flatten() * 255
                rbg_vals =  np.array( roi.resize((OUTPUT_STEPSIZE, OUTPUT_STEPSIZE)) )

                # Track results
                hard_sums[dla_vals.argmax()] += 1

                # Write results
                self.array_write_region_2d        (red,   target_coord, (OUTPUT_STEPSIZE, OUTPUT_STEPSIZE), rbg_vals[:,:,0])
                self.array_write_region_2d        (green, target_coord, (OUTPUT_STEPSIZE, OUTPUT_STEPSIZE), rbg_vals[:,:,1])
                self.array_write_region_2d        (blue,  target_coord, (OUTPUT_STEPSIZE, OUTPUT_STEPSIZE), rbg_vals[:,:,2])
                for chan in channels: 
                    self.array_write_region_2d    (dla[chan],  target_coord, (OUTPUT_STEPSIZE, OUTPUT_STEPSIZE), dla_vals[chan].item())
                pbar.update()
        pbar.close()
        print ("Done generating DLAs")

        hard_sums = 100.0 * hard_sums / sum(hard_sums)
        for chan in channels:
            print ("  | Channel {0} measurement: {1:.2f}%".format(chan + 1, hard_sums[chan]))

        tifffile.imsave('test_RED.tiff', red)
        tifffile.imsave('test_GREEN.tiff', green)
        tifffile.imsave('test_BLUE.tiff', blue)
        print ("Done saving RGB")
        for chan in channels:
            tifffile.imsave('test_DLA{0}.tiff'.format(chan + 1), dla[chan])
        print ("DLA save successful, wrote {0} channels".format(len(channels)))

    def build(self):
        if self.status['CACHE']: return True
        print ("Converting ROIs to numpy arrays ...")

        INPUT_STEPSIZE =  int ( self.params['roi_size'] )
        padding         = int ( self.params['padding']  )

        # Origional image
        tfile = tifffile.TiffFile(self.params['fullpath'])
        target_series = self.get_maxsized_series(tfile)
        img = tfile.asarray(series=target_series)

        print ("Selected series index (guessing to be 40x): ", target_series)
        print ("Selected series size  (guessing to be 40x): ", img.shape)

        roi_data   = []
        roi_coords = []

        INPUT_DIMENSIONS = img.shape
        xsteps = range(0 + padding, INPUT_DIMENSIONS[0] - INPUT_STEPSIZE - padding - 1, INPUT_STEPSIZE) 
        ysteps = range(0 + padding, INPUT_DIMENSIONS[1] - INPUT_STEPSIZE - padding - 1, INPUT_STEPSIZE)
        raster = [ (x, y) for y in ysteps for x in xsteps]
        print ("Length of raster is " + str(len(raster)))

        for i, roi_coord in enumerate(raster):
            roi, data = self.array_read_region(img, roi_coord, (INPUT_STEPSIZE, INPUT_STEPSIZE))
            # Check if there is contrast
            if ImageStat.Stat(roi).stddev[0] > 5    :

                h, s, v = cv2.split(np.asarray(roi.convert('HSV')))
                o = np.where(h > 120, 1, 0)
                o = np.where(v > 50,  o, 0)
                o = np.where(v < 210, o, 0)
                n_pass = np.sum(o)
                if n_pass > 1000:
                    roi_data.append(data)
                    roi_coords.append(roi_coord)

            if (i % (len(raster)//10 )) == 0: print ( " [{2}] Identified {0} ROIs so far and {1:.0f}% done with scan ... ".format( len(roi_coords), 100*(i+1)/len(raster), self.params['basename']))

        np.save(self.params['data_cache'], roi_data)
        np.save(self.params['coor_cache'], roi_coords)
        self.status['CACHE'] = 1 

        return True


    # =============================================================================
    # In-training methods
    def update_resolution_and_buffer(self, resolution):
        '''
        Call this to update the resolution of the output tiles
        Important:
            Must be called before generation!
        Parameters:
            resolution: Integer <= ROI size
            buffer_size: Defunct
        '''

        if not self.status['CACHE']: raise RuntimeWarning("You're updating transforms for an uncached slide, call build() first")
        self.transform_train = transforms.Compose([
            transforms.ToPILImage(),
            transforms.RandomResizedCrop(1024, scale=(0.5, 1.5), ratio=(.8,1.2), interpolation=2),
            transforms.Resize(resolution),
            transforms.RandomHorizontalFlip(p=0.5),
            transforms.RandomVerticalFlip(p=0.5),
            transforms.ColorJitter(brightness=0.2, contrast=0.1, saturation=0.05, hue=0.02),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])

        self.transform_test = transforms.Compose([
            transforms.ToPILImage(),
            transforms.CenterCrop(1024),
            transforms.Resize(resolution),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])
        self.params['crop_to_size'] = 1024
        self.params['resolution'] = resolution
        self.status['READY'] = self.status['CACHE'] 


    def get_train_data(self):
        '''
        Generate data for dataloader
        Tile randomization are in play
        Returns:
            [0] all_data which is a torch tensor of size [Ti, Nc, H, W]
        '''
        if not self.status['READY']: raise RuntimeError("You called get_train_data on a slide that is not ready for tensorization.")

        if  (os.path.isfile(self.params['data_cache'])):
            data  = np.load(self.params['data_cache'])
            random.shuffle(data)
        else:
            raise RuntimeError("Somehow you initialized an RoiBuilder with no cache... quitting", self.params)

        # # Drop tiles randomly...
        # indices = np.random.choice(data.shape[0], int ( data.shape[0] * self.params['dropout'] ), replace=False)
        # data = data[indices]

        # A hard limit...
        data = data[:10]

        if self.params['dtype'] = 'half':
            torch.set_default_tensor_type('torch.HalfTensor')
            all_stacks = torch.stack([self.transform_train(roi).half()  for roi in data])
        if self.params['dtype'] = 'float':
            torch.set_default_tensor_type('torch.FloatTensor')
            all_stacks = torch.stack([self.transform_train(roi).float() for roi in data])
        return all_stacks

    def get_validation_data(self):
        '''
        Generate data for dataloader that returns same results each time
        Tile randomization are in play
        Returns:
            [0] all_data which is a torch tensor of size [Ti, Nc, H, W]
        '''
        if not self.status['READY']: raise RuntimeError("You called get_validation_data on a slide that is not ready for tensorization.")

        if  (os.path.isfile(self.params['data_cache'])):
            data  = np.load(self.params['data_cache'])
        else:
            raise RuntimeError("Somehow you initialized an RoiBuilder with no cache... quitting", self.params)

        if self.params['dtype'] = 'half':
            torch.set_default_tensor_type('torch.HalfTensor')
            all_stacks = torch.stack([self.transform_test(roi).half()  for roi in data])
        if self.params['dtype'] = 'float':
            torch.set_default_tensor_type('torch.FloatTensor')
            all_stacks = torch.stack([self.transform_test(roi).float() for roi in data])
        return all_stacks

    def get_inference_data(self):
        '''
        A method usually called on a single RoiBuilder for plotting/inference purposes 
        No randomization or capping
        Useful for visualization, evaluation, and testing purposes, not recommended for training (where spatial information isn't used)
        Returns:
            [0] all_data which is a torch tensor of size [Ti, Nc, H, W]
            [1] matched ROI coordinates to the tensor index Ti
            [2] the origional numpy data (for imshow, usually)
        '''
        if not self.status['READY']: raise RuntimeError("You called get_inference_data on a slide that is not ready for tensorization.")

        if  (os.path.isfile(self.params['data_cache'])):
            img_data  = np.load(self.params['data_cache'])
            cords = np.load(self.params['coor_cache'])
        else:
            print ("No cache file found... generating first")
            data  = np.load(self.params['data_cache'])
            cords = np.load(self.params['coor_cache'])

        torch.set_default_tensor_type('torch.HalfTensor')
        data = [self.transform_test(roi).half() for roi in img_data]

        all_stacks = torch.stack( data )
        return all_stacks, cords, img_data
