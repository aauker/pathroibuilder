# PathRoiBuilder

A package for interfacing whole slide pathology images to deep-learning platforms and managing relevant metadata.

# Description
The overall goal and idea behind PathRoiBuilder is to define the use of digital pathology images in the context of deep learning as a transforms-based operation. The zeroth level transform is extraction or fetching of data from a pathology slide, that is, generation of tiles from a tiff, svs, scn, or any tiff-based digital pathology filetype. This inloves a raster scan if the transform source is a slide, or a simple load operation if it is a cache.

## Transform Scheme
L0: Slide or Cache --> Array data | Params: tissue filter, step size, tile size

L1: Array data --> Tensors | Params: train and test PyTorch/Tensorflow transform

L2: Tensors --> Tensors | Params: a compatable module (model)

L3: Tensors --> Images | Params: ?

